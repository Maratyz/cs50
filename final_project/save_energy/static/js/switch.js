$( document ).ready( function() {
    $("[name='status-checkbox']").bootstrapSwitch();
    $("[name='track-checkbox']").bootstrapSwitch();
    $('input[name="track-checkbox"]').on('switchChange.bootstrapSwitch', function(event,state) {
        var parameters = {
            appname: this.getAttribute("appname"),
            switch_type: this.getAttribute("name"),
            switch_to: state,
        };
        //console.log(parameters);
        $.getJSON(Flask.url_for("config_switch"), parameters)
            .done(function(data, textStatus, jqXHR) {
                console.log(data);
            })
            .fail(function(jqXHR, textStatus, errorThron) {
                console.log(errorThron.toString());
            });
        //console.log("after getJSON");
    });
});
