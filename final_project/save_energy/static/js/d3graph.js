$( document ).ready( function() {
    //d3.js to generate battery chart
    $.getJSON(Flask.url_for("battery_history"))
        .done(function(data, textStatus, jqXHR) {
            var margin = {top: 20, right: 20, buttom: 30, left: 50},
                width = 400 - margin.left - margin.right,
                height = 300 - margin.top - margin.buttom;

            // set the range
            var x = d3.scaleTime().range([0, width]);
            var y = d3.scaleLinear().range([height, 0]);

            // define the line
            var valueline = d3.line()
                .x(function(d) { return x(d.time_for_js) })
                .y(function(d) { return y(d.percentage)});
            // append the svg object to batterh history chart
            var svg = d3.select("#battery_chart").append("svg")
                .attr("width",width + margin.left+margin.right)
                .attr("height",height + margin.top+margin.buttom)
                .append("g")
                .attr("transform","translate("+margin.left+","+margin.top+")");

            var parseTime = d3.utcParse("%a, %d %b %Y %H:%M:%S GMT")
            data.forEach(function(d) {
                d.time_for_js = parseTime(d.time_for_js);
                d.percentage = d.percentage;
            });
            x.domain(d3.extent(data, function(d) {return d.time_for_js}));
            y.domain([0, d3.max(data, function(d) {return d.percentage})]);
            // Add the value path
            svg.append("path")
                .data([data])
                .attr("class","line")
                .attr("d", valueline);

            svg.append("g")
                .attr("transform","translate(0," + height + ")")
                .call(d3.axisBottom(x));

            svg.append('g')
                .call(d3.axisLeft(y));
        });

    //d3.js to generate tracked chart
    $.getJSON(Flask.url_for("tracked_percentage"))
        .done(function(data, textStatus, jqXHR) {
            var width = 300;
            var height = 300;
            var radius = Math.min(width, height) / 2 - 20;
            var legendRectSize = 18;
            var legendSpacing = 4;

            var color = d3.scaleOrdinal(d3.schemeCategory20c);
            var svg = d3.select('#tracked_app')
                .append('svg')
                .attr('width', width)
                .attr('height', height)
                .append('g')
                .attr('transform', 'translate(' + (width / 2) +  ',' + (height / 2) + ')');
            var arc = d3.arc()
                .innerRadius(100)
                .outerRadius(radius);
            var pie = d3.pie()
                .value(function(d) { return d.count; })
            var path = svg.selectAll('path')
                .data(pie(data))
                .enter()
                .append('path')
                .attr('d', arc)
                .attr('fill', function(d, i) {
                    return color(d.data.label);
                })
            var legend = svg.selectAll('.legend')
                .data(color.domain())
                .enter()
                .append('g')
                .attr('class', 'legend')
                .attr('transform', function(d, i) {
                    var height = legendRectSize + legendSpacing;
                    var offset =  height * color.domain().length / 2;
                    var horz = -2 * legendRectSize;
                    var vert = i * height - offset;
                    return 'translate(' + horz + ',' + vert + ')';
                });
            legend.append('rect')
                .attr('width', legendRectSize)
                .attr('height', legendRectSize)
                .style('fill', color)
                .style('stroke', color);
            legend.append('text')
                .attr('x', legendRectSize + legendSpacing)
                .attr('y', legendRectSize - legendSpacing)
                .text(function(d) { return d; });
        });

} )
