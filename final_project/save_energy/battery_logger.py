import sys
import os
import subprocess
import re
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func
from save_energy.application_config import *
from save_energy import db
""" to be removed if 'from config_web import app, db' works
app = Flask(__name__)
dbname = "appTracker.db"

app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///" + dbname
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["SQLALCHEMY_ECHO"] = False

db = SQLAlchemy(app)
"""
class battery_log(db.Model):
    __tablename__ = "battery_log"
    record_id = db.Column(db.Integer, primary_key=True, )
    timestamp = db.Column(db.DateTime, nullable=False, server_default=func.now())
    powersource = db.Column(db.Integer, nullable=False) #0 for AC 1 for battery
    percentage = db.Column(db.Numeric, nullable=False)

    def __repr__(self):
        return "timestamp:{},powersource:{},powerpercentage:{}%".format(self.timestamp,self.powersource,self.percentage)

class battery_tracker:
    current_powersource = ""
    current_power_percentage = 0
    def update(self):
        message_from_pmset = subprocess.Popen(['pmset','-g','batt'], stdout=subprocess.PIPE).stdout.readlines()
        for each in message_from_pmset:
            each = each.decode('utf8').rstrip()
            if "Now drawing from" in each:
                self.current_powersource = re.search("'(.*)'", each).group(1)
            if "InternalBattery" in each:
                each = each.split()
                self.current_power_percentage = int(each[2].rstrip("%;"))
        update_message = battery_log(timestamp=func.now(),powersource=self.current_powersource, percentage=self.current_power_percentage)
        db.session.add(update_message)
        db.session.commit()

