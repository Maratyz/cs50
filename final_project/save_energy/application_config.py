pid = ('/tmp/cloud_service_management.pid')
update_period = 60
home_ssid = "" # put home SSID in

""" logger configuration"""
import logging
import sys
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logger.propagate = False
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh = logging.FileHandler("/tmp/cloud_service_management.log", "w")
fh.setLevel(logging.INFO) #INFO, DEBUG or ERROR
fh.setFormatter(formatter)

ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.INFO)
ch.setFormatter(formatter)
logger.addHandler(fh)
logger.addHandler(ch)
keep_fds = [fh.stream.fileno()]

""" database configuration """
dbname = "appTracker.db"
