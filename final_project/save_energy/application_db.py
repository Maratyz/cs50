import sys
import subprocess
from wireless import Wireless
import logging

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func

from save_energy.application_config import *
from save_energy import db
""" to be removed if 'from config_web import app, db' works
app = Flask(__name__)
dbname = "appTracker.db"

app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///" + dbname
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["SQLALCHEMY_ECHO"] = False

db = SQLAlchemy(app)
"""
class appTracker(db.Model):
    __tablename__ = "applicationConfiguration"

    id = db.Column(db.Integer, primary_key = True)
    appname = db.Column(db.String)
    status = db.Column(db.Integer) #0: inactive 1: active
    tracked = db.Column(db.Integer) #0: not, 1: yes
    _updatetime = db.Column(db.DateTime, nullable=False, server_default=func.now())

    def __repr__(self):
        return "id:{},appname:{},status:{},tracked:{},updatetime:{}".format(self.id, self.appname, self.status, self.tracked, self.updatetime)

class cloud_manage_synchronization:
    def __init__(self, home_ssid= ""):
        self.home_ssid = home_ssid
        self._active_processes = subprocess.Popen(['ps','aux'], stdout=subprocess.PIPE).stdout.readlines()
        self._application_list = subprocess.Popen(['ls','/Applications'], stdout=subprocess.PIPE).stdout.readlines()
        self.active_app_set = set()

    def current_active_app_db(self):
        for each in self._active_processes:
            app = each.decode('utf8').rstrip().split()[10:] # take the last column in ps aux
            app = ' '.join(app) # join the space back
            if "Applications" in app:
                app = app.split("/")
                if app[4] == "MacOS": # "MacOS" in app: exclude plugins
                    app = app[2].replace(".app","") # take the second as the application name
                    self.active_app_set.add(app)
        app_list = db.session.query(appTracker.appname)
        for each in app_list:
            if each.appname in self.active_app_set:
                app_status_update = appTracker.query.filter_by(appname=each.appname).first()
                app_status_update.status=1
                app_status_update._updatetime=func.now()
                db.session.add(app_status_update)
            else:
                app_status_update = appTracker.query.filter_by(appname=each.appname).first()
                app_status_update.status=0
                app_status_update._updatetime=func.now()
                db.session.add(app_status_update)
                #db.session.add(app_status_update)
        db.session.commit()

    def update_app_list_into_db(self):
        for each in self._application_list:
            app = each.decode('utf8').rstrip().replace(".app","")
            exist = db.session.query(appTracker.appname).filter_by(appname=app).scalar()
            if exist is None:
                add_an_new_app = appTracker(appname=app,status=0,tracked=0,_updatetime=func.now())
                db.session.add(add_an_new_app)
                logger.debug("Add "+app+" to database "+appTracker.__tablename__)
        db.session.commit()


    def update_db(self):
        _wireless = Wireless()
        _current_ssid = _wireless.current()
        app_list = db.session.query(appTracker.appname, appTracker.status, appTracker.tracked)
        if (_current_ssid != self.home_ssid):
            logger.debug("Not at home, close high-bandwidth applications")
            for each in app_list:
                if each.tracked == 1 and each.status == 1:
                    logger.info(each.appname + " active, to be close")
                    self.application_operation(each.appname, "quit")
                    app_status_update = appTracker.query.filter_by(appname=each.appname).first()
                    app_status_update.status=0
                    app_status_update._updatetime=func.now()
                    db.session.add(app_status_update)
                elif each.tracked == 0:
                    logger.debug(each.appname + " not in track list, no further action")
                else:
                    logger.debug(each.appname + " inactive, no further action")
            db.session.commit()
        elif (_current_ssid == self.home_ssid):
            logger.debug("At home, to active synchronization applications")
            for each in app_list:
                if each.tracked == 1 and each.status == 0:
                    logger.info(each.appname + " inactive, to be activated")
                    self.application_operation(each.appname, "activate")
                    app_status_update = appTracker.query.filter_by(appname=each.appname).first()
                    app_status_update.status=1
                    app_status_update._updatetime=func.now()
                    db.session.add(app_status_update)
                elif each.tracked == 0:
                    logger.debug(each.appname + " not in track list, no further action")
                else:
                    logger.debug(each.appname + " active, no fruther action")
            db.session.commit()
        else:
            logger.debug("Wireless cannot be detected, no further action to be taken")
        db.session.remove()

    def application_operation(self, app, operation):
        # operation: quit or activate
        operation_command = str('tell application "' + app + '" to ' + operation )
        logger.debug(operation_command)
        subprocess.call(['osascript', '-e', operation_command])

