import sys
import csv
import urllib.request
import subprocess
from wireless import Wireless
import logging

from flask_jsglue import JSGlue
from flask import Flask, jsonify,redirect, render_template, request, session, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_session import Session
from sqlalchemy.sql import func
#from application_db import appTracker
from save_energy.application_config import *

import datetime, time

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///" + dbname
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["SQLALCHEMY_ECHO"] = False
db = SQLAlchemy(app)
from save_energy.application_db import appTracker
from save_energy.battery_logger import battery_log
jsglue = JSGlue(app)

@app.route("/")
def index():
    app_list = db.session.query(appTracker.appname, appTracker.status, appTracker.tracked).order_by(appTracker.tracked.desc(), appTracker.status.desc()).all()
    return render_template("index.html", app_list=app_list)

def update_status(app_update, switch_type, switch_to):
    switch_result = "False"
    if switch_type == "status-checkbox":
        if switch_to == "false":
            app_update.status = 0
            db.session.add(app_update)
            switch_result="Success"
        elif switch_to == "true":
            app_update.status = 1
            db.session.add(app_update)
            switch_result="Success"
        else:
            logger.info("unknown status update")
    elif switch_type == "track-checkbox":
        if switch_to == "false":
            app_update.tracked = 0
            db.session.add(app_update)
            switch_result="Success"
        elif switch_to == "true":
            app_update.tracked = 1
            db.session.add(app_update)
            switch_result="Success"
        else:
            logger.info("unknown status update")
    db.session.commit()
    return switch_result

@app.route("/config_switch")
def config_switch():
    appname = request.args.get('appname')
    switch_to = request.args.get('switch_to')
    switch_type = request.args.get('switch_type')
    logger.info(appname + switch_type + " configured to be " + switch_to)
    app_update = db.session.query(appTracker).filter_by(appname=appname).first()
    update_result = update_status(app_update, switch_type, switch_to)
    return jsonify({"status": update_result})


@app.route("/battery_history")
def battery_history():
    battery_history = db.session.query(battery_log).order_by(battery_log.record_id.desc()).limit(360)
    battery_history_list = []
    for each in battery_history:
        time_for_js = int(time.mktime(each.timestamp.timetuple()))*1000
        battery_history_list.append({"time_for_js": each.timestamp, "percentage": int(each.percentage)})
    return jsonify(battery_history_list)

@app.route("/tracked_percentage")
def tracked_percentage():
    not_tracked = db.session.query(appTracker.appname).filter(appTracker.tracked!=1).count()
    tracked = db.session.query(appTracker.appname).filter(appTracker.tracked==1).count()
    return jsonify([{"label": "tracked", "count": tracked}, { "label": "notracked", "count": not_tracked}])

