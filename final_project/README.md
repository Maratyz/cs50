## tl;dr
1. Install
2. Configure
3. Execute

## Background
This python program is used to switch applications once installed on a macos. I wrote it for personal usage and also for the final project of cs50, a great class for anyone who is interested in computer science.

## Installation
git clone the repository to a directory which you feel comfortable

```
pip install -e .
```

## Usage

### Configure the applicaitons to be tracked
edit the home_ssid = "YOUR HOME SSID"
```
export FLASK=save_energy
run flask
```

### Execute the service
```
python application.py
```

## Issues
Any advice is welcome. I do hope to continue develop this small utility.

### Contact
I dislike facebook, twitter is more transparent. 
[twitter](https://twitter.com/yenzhit)
[medium](https://medium.com/manjeaneer)
