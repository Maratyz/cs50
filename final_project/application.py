import os
import sys
import time
import subprocess
import logging
import datetime
from wireless import Wireless
from daemonize import Daemonize
from time import sleep
from config_web.application_db import *
from config_web.application_config import *
from config_web.battery_logger import *


def main():
    logger.debug("Start")
    #logger.debug("Current time: " + str(datetime.datetime.now()))
    cloud_manager = cloud_manage_synchronization(home_ssid)
    cloud_manager.update_app_list_into_db()
    cloud_manager.current_active_app_db()
    """
    cloud_manager.update_db()
    batter_tracker = battery_tracker()
    batter_tracker.update()
    """

    while True:
        logger.info("Update started")
        cloud_manager = cloud_manage_synchronization(home_ssid)
        cloud_manager.update_db()
        batter_tracker = battery_tracker()
        batter_tracker.update()
        logger.info("Update completed")
        sleep(update_period)

daemon = Daemonize(app="cloud_manage_synchronization", pid=pid, action=main, keep_fds=keep_fds)
daemon.start()

if __name__ == "__main__":
    main()
