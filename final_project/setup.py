from setuptools import setup
import sys

if sys.version_info[:2] < (3,6):
    raise RuntimeError("The application tracker requires python >= 3.6")

setup(
        name='save_energy',
        version='1.0',
        description='Python application for switching applications upon SSID',
        author='Marat Chen',
        author_email='yenzhi@gmail.com',
        packages=['save_energy'],
        include_package_data=True,
        install_requires=[
            'flask',
            'wireless',
            'flask_sqlalchemy',
            'sqlalchemy',
            ],
        )

# start initialization of a sqlite database in config_web
from save_energy.application_db import appTracker
from save_energy.battery_logger import battery_log
from save_energy import app, db

print("hello installer")
with app.app_context():
    db.create_all()

# initialization completed
